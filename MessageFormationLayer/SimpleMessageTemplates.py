


MessageTemplates={

    "simple":{
        "createMessageGreeting":{
            "basic":[
                "Hey I am Sam ! <br> I am here to help you"
            ],

            "namePresent": [
                "Thank you! Bye!",

            ],
            "askForUserForm": [
                "Awesome, please fill in your details and we will get back to you soonest.",

            ],
            "CatPresent": [
                "Please select what would you like to do"
            ],
            "catSelected":[
             "It's great that you have selected <b>{0}</b>."
            ],
            "selectRegions": [
                "Please select the regions"

            ],
}
    }
}

