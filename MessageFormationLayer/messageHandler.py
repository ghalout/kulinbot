
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# from django.db import models
from django.conf import settings
import random

# import random
# Create your models here.
from SimpleMessageTemplates import MessageTemplates as SimpleMessageTemplates
SimpleMessageTemplates = SimpleMessageTemplates[settings.BOT_PERSONA]
from DataAccessLayer.dbHandler import getAsianBotData





#Different type of componets/inputs are defined here
def createMessage(uiComponentTrayFromDecisionLayer):

    updatedComponents = []

    componentProcessMap = {
        "SimpleMessage": processSimpleMessage,
      
        "Bubble": processBubble,
        "Form": processForm,
       
    }

    for component in uiComponentTrayFromDecisionLayer["uiComponents"]:
        #
        # based on component type select the specific processing function from component process map and
        # update the component
        #
        componentType = component["componentType"]
        updatedComponent = componentProcessMap[componentType](component)

        # append the updated components

        updatedComponents.append(updatedComponent)

    uiComponentTrayFromMessageLayer = {}
    uiComponentTrayFromMessageLayer["uiComponents"] = updatedComponents
    uiComponentTrayFromMessageLayer["botResponse"] = uiComponentTrayFromDecisionLayer["botResponse"]

    return uiComponentTrayFromMessageLayer

#Simple messages are processed here
def processSimpleMessage(simpleMessageComponent):

    if simpleMessageComponent["tag"]=="createMessageGreeting.basic":
        simpleMessageComponent["message"]= random.choice(SimpleMessageTemplates["createMessageGreeting"]["basic"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.namePresent":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["namePresent"]).format(simpleMessageComponent["data"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.askForUserForm":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["askForUserForm"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.CatPresent":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["CatPresent"]).format(
            simpleMessageComponent["data"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.catSelected":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["catSelected"]).format(
            simpleMessageComponent["data"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.selectRegions":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["selectRegions"])
    return simpleMessageComponent

#UserForm is processed
def processForm(UserFormComponent):
    if UserFormComponent["tag"] == "UserDetails":
        UserFormComponent["nextCallTag"] = "Store_User_Details"
        UserFormComponent["message"] = ""
    return UserFormComponent

def processBubble(Bubble):
    print Bubble

    if Bubble["tag"] == "createBotData.data":
        print Bubble["data"],'test test test'
        Bubble["data"] = getAsianBotData(Bubble["websiteId"], Bubble["parentId"])
        Bubble["nextCallTag"] = "Store_Bubble_data"


    return Bubble
   
  